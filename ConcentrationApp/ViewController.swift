//
//  ViewController.swift
//  ConcentrationApp
//
//  Created by Aryan Arora on 2018-05-29.
//  Copyright © 2018 Aryan Arora. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var flipCount :Int = 0 {
        didSet {   //everytime flipcOUNT GETS CHNAGED , this function is executed
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }

    @IBOutlet var cardButtons: [UIButton]!
    @IBAction func touchCard(_ sender: UIButton) {
        flipCount += 1
       // flipCountLabel.text = "Flips: \(flipCount)"
        if let cardNumber = cardButtons.index(of: sender) {
             print("Card Number : \(cardNumber)")
            flipCard(withEmoji: sender.currentTitle!, on: sender)
        }
        else{
            print("Card clicked is not in the button array")
        }
       
    }
    
    
//    @IBAction func touchCard2(_ sender: UIButton) {
//        flipCount += 1
//        // flipCountLabel.text = "Flips: \(flipCount)"
//        flipCard(withEmoji: "🎃", on: sender)
//    }
    
    
    func flipCard(withEmoji emoji: String, on button: UIButton) {
        print("Flipped Card with:  \(emoji)")
        if button.currentTitle == emoji {
            button.setTitle("", for: UIControlState.normal)
            button.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
        }
        else    {
            button.setTitle(emoji, for: UIControlState.normal)
            button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
    }// external name (argv name when calling the func) internal name (argv name when implementing the func)
    

    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    
    
    
}

